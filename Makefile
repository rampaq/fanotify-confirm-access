KERNEL_SOURCE := /usr/src/linux-headers-$(shell uname -r)

#CPPFLAGS := -I$(KERNEL_SOURCE)/include -D_GNU_SOURCE
CPPFLAGS := -D_GNU_SOURCE
CFLAGS := -g -Wall -W -Wextra -O2 
LDFLAGS := $(CFLAGS)  -ldialog -lncursesw -lm

all: fanotify

fanotify: fanotify.o
	$(CC) fanotify.o $(LDFLAGS) -o fanotify

fanotify.c: $(KERNEL_SOURCE)/include/linux/fanotify.h fanotify-syscalllib.h

clean:
	rm -f fanotify fanotify.o *.orig *.rej
